import os

# 5.3 Flask-SQLAlchemy configuration
basedir = os.path.abspath(os.path.dirname(__file__))


# 3.1 Secret key configuration--the value of an environment variable or a hardcoded string.
# This is a pattern that you will see me repeat often for configuration variables.
# development or production

class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'you-will-never-guess'  # 3.1

    #  4.3 Flask-SQLAlchemy configuration
    #     # The Flask-SQLAlchemy extension takes the location of the application's database from the SQLALCHEMY_DATABASE_URI configuration variable.
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
                              'sqlite:///' + os.path.join(basedir, 'app.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # 7b asynchronous email
    # MAIL_SERVER = "smtp.163.com"
    # MAIL_PORT = 25
    # MAIL_USE_TLS = True
    # MAIL_USE_SSL = False
    # MAIL_USERNAME = "sxtyzrd@163.com"
    # MAIL_PASSWORD = "CGFDXBMAIQCDPPLR"

    # 7.4 email configuration
    # If the email server is not set in the environment, then I will use that as a sign that emailing errors needs to be disabled.
    MAIL_SERVER = os.environ.get('MAIL_SERVER')
    MAIL_PORT = int(os.environ.get('MAIL_PORT') or 25)
    MAIL_USE_TLS = os.environ.get('MAIL_USE_TLS') is not None
    MAIL_USERNAME = os.environ.get('MAIL_USERNAME')
    MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD')
    ADMINS = ['sxtyzrd@163.com']

    # 9.4 Posts per page configuration
    POSTS_PER_PAGE = 3

    #
    APPID = os.environ.get('APPID')
    BD_TRANSLATOR_KEY = os.environ.get('BD_TRANSLATOR_KEY')

    SECRET_KEY = os.environ.get('SECRET_KEY') or "my secret key"

    LANGUAGES = ['en', 'zh']
