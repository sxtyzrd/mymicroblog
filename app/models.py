from datetime import datetime
from hashlib import md5
from time import time

import jwt
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash

from app import db, login, app

# 8.3 followers association table
followers = db.Table('followers',
    db.Column('follower_id', db.Integer, db.ForeignKey('user.id')),
    db.Column('followed_id', db.Integer, db.ForeignKey('user.id'))
)


# 4.4 Database Models--User database model
# class User(db.Model):
#  5.3 Flask-Login user mixin class
class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    #  4.8 Posts database table and relationship
    posts = db.relationship('Post', backref='author', lazy='dynamic')
    # 6.4 extend the users table with 2 new fields
    about_me = db.Column(db.String(140))
    last_seen = db.Column(db.DateTime, default=datetime.utcnow)

    # 8.3 declare many-to-many followers relationship
    followed = db.relationship(
        'User', secondary=followers,
        primaryjoin=(followers.c.follower_id == id),
        secondaryjoin=(followers.c.followed_id == id),
        backref=db.backref('followers', lazy='dynamic'), lazy='dynamic')

    def __repr__(self):
        return '<User {}>'.format(self.username)

    # 5.1 Password hashing and verification
    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    # 5.1 Password hashing and verification
    def check_password(self, password):
        return check_password_hash(self.password_hash, password)


    # 6.2 generate avatar URL
    def avatar(self, size):
        digest = md5(self.email.lower().encode('utf-8')).hexdigest()
        return 'https://www.gravatar.com/avatar/{}?d=identicon&s={}'.format(
            digest, size)

    # 8.4 add and remove followers, is_following() supporting method
    def follow(self, user):
        if not self.is_following(user):
            self.followed.append(user)

    def unfollow(self, user):
        if self.is_following(user):
            self.followed.remove(user)
    # looks for items in the association table that have the left side foreign key set to the self user, and the right side set to the user argument.
    def is_following(self, user):
        return self.followed.filter(
            followers.c.followed_id == user.id).count() > 0

    # 8.5 Followed posts query
    # def followed_posts(self):
    #     return Post.query.join(
    #         followers, (followers.c.followed_id == Post.user_id)).filter(
    #         followers.c.follower_id == self.id).order_by(
    #         Post.timestamp.desc())

    # 8.6  Followed posts query with user's own posts.
    def followed_posts(self):
        followed = Post.query.join(
            followers, (followers.c.followed_id == Post.user_id)).filter(
            followers.c.follower_id == self.id)
        own = Post.query.filter_by(user_id=self.id)
        return followed.union(own).order_by(Post.timestamp.desc())

    # 10.5 reset password token methods--get return a token, verfy
    def get_reset_password_token(self, expires_in=600):
        return jwt.encode(
            {'reset_password': self.id, 'exp': time() + expires_in},
            app.config['SECRET_KEY'], algorithm='HS256')

    @staticmethod
    def verify_reset_password_token(token):
        try:
            id = jwt.decode(token, app.config['SECRET_KEY'],
                            algorithms=['HS256'])['reset_password']
        except:
            return
        return User.query.get(id)

# 4.4 Database Models--
class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.String(140))
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    language = db.Column(db.String(5))  # 14.3 Add detected language to Post model

    def __repr__(self):
        return '<Post {}>'.format(self.body)

# 5.4 Flask-Login user loader function
# The user loader is registered with Flask-Login with the @login.user_loader decorator.
# convert the string to integer
@login.user_loader
def load_user(id):
    return User.query.get(int(id))

